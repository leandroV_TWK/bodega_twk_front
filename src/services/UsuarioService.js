import { store } from "../store"

class UsuarioService {


    getUsuarios() {
        return fetch('http://127.0.0.1:8000/api/usuario',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    getUsuario(id) {
        return fetch(`http://127.0.0.1:8000/api/usuario/${id}`,{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                if (json) {
                    store.perfilItem = json
                }
            })
    }


    infoUsuario(dato) {
        return fetch(`http://127.0.0.1:8000/api/infousuario/${dato.id}`,{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })            
    }

    putUsuario(datos) {
        return fetch(`http://127.0.0.1:8000/api/usuario/${datos.id}`, {
            method: 'PUT', // or 'POST'
            body: JSON.stringify(datos), // Los datos pueden ser un string o un {objeto}
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    postUsuario(datos) {
        return fetch('http://127.0.0.1:8000/api/usuario', {
            method: 'POST',
            body: JSON.stringify(datos), // Los datos pueden ser un string o un {objeto}
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    deleteUsuario(datos) {
        return fetch(`http://127.0.0.1:8000/api/usuario/${datos.id}`, {
            method: 'DELETE',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })

    }


}

export default new UsuarioService();