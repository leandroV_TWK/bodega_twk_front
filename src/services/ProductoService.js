import { store } from "../store"

class ProductoService {

    getProductos() {
        return fetch('http://127.0.0.1:8000/api/producto',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    getProducto(id) {
        return fetch(`http://127.0.0.1:8000/api/producto/${id}`,{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                if (json) {
                    store.perfilItem = json
                }
            })
    }

    infoProducto(dato) {
        return fetch(`http://127.0.0.1:8000/api/infoproducto/${dato.id}`,{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })            
    }

    putProducto(datos) {
        return fetch(`http://127.0.0.1:8000/api/producto/${datos.id}`, {
            method: 'PUT', // or 'POST'
            body: JSON.stringify(datos), // Los datos pueden ser un string o un {objeto}
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    postProducto(datos) {
        return fetch('http://127.0.0.1:8000/api/producto', {
            method: 'POST',
            body: JSON.stringify(datos), // Los datos pueden ser un string o un {objeto}
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    deleteProducto(datos) {
        return fetch(`http://127.0.0.1:8000/api/producto/${datos.id}`, {
            method: 'DELETE',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    getProductoCategoria(id){
        return fetch(`http://127.0.0.1:8000/api/procat/${id}`,{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })        
    }

}

export default new ProductoService();