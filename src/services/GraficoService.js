
import {store} from '../store'

class GraficoService {

    getBarra() {
        return fetch('http://127.0.0.1:8000/api/barra',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }
    getPie() {
        return fetch('http://127.0.0.1:8000/api/pie',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    totalProducto() {
        return fetch('http://127.0.0.1:8000/api/totalpro',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })
    }

    totalCategoria() {
        return fetch('http://127.0.0.1:8000/api/totalcat',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })
    }
    totalBodega() {
        return fetch('http://127.0.0.1:8000/api/totalbod',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })
    }
    totalSucursal() {
        return fetch('http://127.0.0.1:8000/api/totalsuc',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })
    }
    totalUsuario() {
        return fetch('http://127.0.0.1:8000/api/totalusu',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })
    }
}

export default new GraficoService();