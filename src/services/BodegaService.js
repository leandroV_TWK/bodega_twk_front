import { store } from "../store"

class BodegaService {

    getBodegas() {
        return fetch('http://127.0.0.1:8000/api/bodega', {
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    getBodega(id) {
        return fetch(`http://127.0.0.1:8000/api/bodega/${id}`, {
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                if (json) {
                    store.perfilItem = json
                }
            })
    }

    infoBodega(dato) {
        return fetch(`http://127.0.0.1:8000/api/infobodega/${dato.id}`,{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })            
    }

    putBodega(datos) {
        return fetch(`http://127.0.0.1:8000/api/bodega/${datos.id}`, {
            method: 'PUT', // or 'POST'
            body: JSON.stringify(datos), // Los datos pueden ser un string o un {objeto}
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    postBodega(datos) {
        return fetch('http://127.0.0.1:8000/api/bodega', {
            method: 'POST',
            body: JSON.stringify(datos), // Los datos pueden ser un string o un {objeto}
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    deleteBodega(datos) {
        return fetch(`http://127.0.0.1:8000/api/bodega/${datos.id}`, {
            method: 'DELETE',
        })
    }

}

export default new BodegaService();