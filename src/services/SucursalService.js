import { store } from "../store"

class SucursalService {

    getSucursales() {
        return fetch('http://127.0.0.1:8000/api/sucursal',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    getSucursal(id) {
        return fetch(`http://127.0.0.1:8000/api/sucursal/${id}`,{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                if (json) {
                    store.perfilItem = json
                }
            })
    }

    infoSucursal(dato) {
        return fetch(`http://127.0.0.1:8000/api/infosucursal/${dato.id}`,{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })            
    }

    putSucursal(datos) {
        return fetch(`http://127.0.0.1:8000/api/sucursal/${datos.id}`, {
            method: 'PUT', // or 'POST'
            body: JSON.stringify(datos), // Los datos pueden ser un string o un {objeto}
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    postSucursal(datos) {
        return fetch('http://127.0.0.1:8000/api/sucursal', {
            method: 'POST',
            body: JSON.stringify(datos), // Los datos pueden ser un string o un {objeto}
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    deleteSucursal(datos) {
        return fetch(`http://127.0.0.1:8000/api/sucursal/${datos.id}`, {
            method: 'DELETE',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

}
export default new SucursalService();