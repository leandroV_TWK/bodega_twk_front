
import { store } from '../store';

class AuthService {

    login(credenciales) {
        return fetch('http://127.0.0.1:8000/api/login', {
            method: 'post',
            mode: 'cors',
            credentials: "omit",
            headers: new Headers({
                'Content-Type': 'application/json'
            }),
            body: JSON.stringify({
                "email": credenciales.email,
                "password": credenciales.password
            })
        })
            .then((response) => {
                return response.json();
            })
            .then(function (data) {
                if (data.status_code == 200) {
                    store.token_auth = data.token;
                    store.conectado = true;
                    store.tipoUsuario = data.tipo;
                }
            })
    }


    logout() {
        return fetch('http://127.0.0.1:8000/api/logout', {
            method: 'post',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then(() => {
                store.conectado = false;
            })
    }

    usuarioActivo() {        // PROBAR NECESIDAD DE CONSULTAR POR EL USUARIO LOGUEADO
        return fetch('http://127.0.0.1:8000/api/usuactual', {
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
            .then((response) => {
                return response.json();
              })
              .then((json) => {
                /* if (json.status_code == 400) {            
                  this.datos = json;
                } */console.log(json);
              })
        })
    }
}

export default new AuthService();