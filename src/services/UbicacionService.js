
import {store} from '../store'

class UbicacionService {

    getRegiones() {
        return fetch('http://127.0.0.1:8000/api/region',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    getComunas() {
        return fetch('http://127.0.0.1:8000/api/comuna',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

}

export default new UbicacionService();