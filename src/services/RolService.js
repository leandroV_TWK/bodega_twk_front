
import {store} from '../store'

class RolService {

    getRoles() {
        return fetch('http://127.0.0.1:8000/api/rol',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    getRol(id) {
        return fetch(`http://127.0.0.1:8000/api/rol/${id}`,{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

}

export default new RolService();