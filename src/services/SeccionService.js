import { store } from "../store"

class SeccionService {

    getSecciones() {
        return fetch('http://127.0.0.1:8000/api/categoria',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    postSeccion(datos) {
        return fetch('http://127.0.0.1:8000/api/categoria', {
            method: 'POST',
            body: JSON.stringify(datos), // Los datos pueden ser un string o un {objeto}
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        });

    }

    getSeccion(id) {
        fetch(`http://127.0.0.1:8000/api/categoria/${id}`,{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
            .then((response) => {
                return response.json();
            })
            .then((json) => {
                if (json) {
                    store.perfilItem = json
                }
            })
    }

    

    putSeccion(datos) {
        return fetch(`http://127.0.0.1:8000/api/categoria/${datos.id}`, {
            method: 'PUT', // or 'POST'
            body: JSON.stringify(datos), // Los datos pueden ser un string o un {objeto}
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        });
    }

    deleteSeccion(datos) {
        return fetch(`http://127.0.0.1:8000/api/categoria/${datos.id}`, {
            method: 'DELETE',
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
        /* .then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(console.log('Eliminado exitosamente')); */
    }
}
export default new SeccionService();