import { store } from "../store";

class VentaService {

    postVenta(datos) {
        return fetch('http://127.0.0.1:8000/api/venta', {
            method: 'POST',
            body: JSON.stringify(datos), // Los datos pueden ser un string o un {objeto}
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

    postProductoVenta(datos) {
        return fetch('http://127.0.0.1:8000/api/proven', {
            method: 'POST',
            body: JSON.stringify(datos), // Los datos pueden ser un string o un {objeto}
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

}
export default new VentaService();