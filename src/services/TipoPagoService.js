import { store } from "../store"

class TipoPagoService{

    getTiposPago() {
        return fetch('http://127.0.0.1:8000/api/tipopago',{
            headers: new Headers({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + store.token_auth
            })
        })
    }

}
export default new TipoPagoService();