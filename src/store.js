import Vue from 'vue'

export const store = Vue.observable({
    token_auth: null,
    conectado: false,
    tipoUsuario: 0, 
    perfilItem: null, // Usado para edición de registros
    crearModal: false, 
    editarModal: false,
    verModal: false,
    objeto: {}, // Para controlar creación
    snackbarText: "",
});

export const objeto = {
    alterarObjeto(item) {
        store.objeto = item;
    },
    limpiarObjeto() {
        store.objeto = {};
    }
};


export const auth = {
    verificar(usuario, contraseña) { // Esto debe hacerse en backend (autenticar) menos estado conectado
        store.usuario = usuario;
        store.contraseña = contraseña;
        store.conectado = true;
    }
};