import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/login/Login.vue'
import Usuario from '../components/usuario/Usuario.vue'
import Dashboard from '../components/dashboard/Dashboard.vue'
import Seccion from '../components/seccion/Seccion.vue'
import Sucursal from '../components/sucursal/Sucursal.vue'
import Producto from '../components/producto/Producto.vue'
import Bodega from '../components/bodega/Bodega.vue'
import Venta from '../components/venta/Venta.vue'
import { store } from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
    meta: { requiereAuth: true }
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard,
    meta: { requiereAuth: true }
  },
  {
    path: '/usuario',
    name: 'Usuario',
    component: Usuario,
    meta: { requiereAuth: true }
  },
  {
    path: '/seccion',
    name: 'Seccion',
    component: Seccion,
    meta: { requiereAuth: true }
  },
  {
    path: '/sucursal',
    name: 'Sucursal',
    component: Sucursal,
    meta: { requiereAuth: true }
  },
  {
    path: '/producto',
    name: 'Producto',
    component: Producto,
    meta: { requiereAuth: true }
  },
  {
    path: '/bodega',
    name: 'Bodega',
    component: Bodega,
    meta: { requiereAuth: true }
  },
  {
    path: '/venta',
    name: 'Venta',
    component: Venta,
    meta: { requiereAuth: true }
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// Global "antes del guard" 
//se llaman en el orden de creación, siempre que 
//se activa una navegación. Los guards se pueden 
//resolver de forma asincrónica y la navegación se 
//considera pendiente antes de que se hayan resuelto todos los hooks.
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiereAuth)) {  //La ruta requiere autenticar

    if (to.name != 'Login' && !store.conectado) {
      next({
        name: 'Login'
      })
    }
    else {
      next();
    }

  }

});

export default router
